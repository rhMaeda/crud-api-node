
const express = require('express');

const monk = require('monk');
const Joi = require('@hapi/joi');

const db = monk(process.env.MONGO_URI);
const faqs = db.get('faqs');

const schema = Joi.object({
    pergunta: Joi.string().trim().required(),
    resposta: Joi.string().trim().required(),
    video_url: Joi.string().uri(),
})


const router = express.Router();

//ler tudo
router.get('/',async (request, response, next) => {
    response.json({
        msg: "Lendo todos",
    })
    try{
        const items = await faqs.find({});
        response.json(items);
    } catch(error){
        next(error);
    }
})
//ler um
router.get('/:id',async (request, response, next) => {
    // response.json({
    //     msg: "Lendo um",
    // })
    try{
        const { id } = request.params;
        const item = await faqs.findOne({
            _id: id,
        });
        if( !item ) return next();
        return response.json(item)
    }catch(error){
        next(error);

    }
})

//criar
router.post('/',async  (request, response, next) => {
    // response.json({
    //     msg: "Criando",
    // })
    try{
        // console.log(request.body)
        const value = await schema.validateAsync(request.body)
       // response.json(value);
        const inserted = await faqs.insert(value);
        response.json(inserted);
    }catch(error){
        next(error);
    }
})
// atualizar
router.put('/:id',async (request, response, next) => {
    // response.json({
    //     msg: "Atualizando",
    // })
    try{
        const { id } = request.params;
        const value = await schema.validateAsync(request.body)
        const item = await faqs.findOne({
            _id: id,
        });
        if(!item) return next();
        
        //const updated = await faqs.update({
        await faqs.update({
            _id: id,
        }, {
            $set: value,
        });
        response.json(value);
    }catch(error){
        next(error);
    }
})
// delete
router.delete('/:id',async (request, response, next) => {
    // response.json({
    //     msg: "Deletando",
    // })
    try{
        const {id} = request.params;
        await faqs.remove({_id:id});
        response.status(200).send("Deletado com sucesso");
    } catch(error){
        next(error);
    }
})

module.exports = router;